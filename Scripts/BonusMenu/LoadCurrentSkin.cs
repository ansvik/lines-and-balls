﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadCurrentSkin : MonoBehaviour
{

    public ColorBlock cb;

    void Start()
    {
        string skin = PlayerPrefs.GetString("Skin");
        GameObject go = GameObject.Find(skin);
        go.GetComponent<Button>().colors = cb;
    }

    void Update()
    {
        
    }
}
