﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSkinButton : MonoBehaviour
{
    public ColorBlock cb;

    public void Click(string name)
    {
        PlayerPrefs.SetString("Skin", name);
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("SkinButton");
        foreach (GameObject button in buttons)
        {
            if (button.name == name)
            {
                button.GetComponent<Button>().colors = cb;
            }
            else
            {
                button.GetComponent<Button>().colors = ColorBlock.defaultColorBlock;
            }
        }
        Debug.Log(PlayerPrefs.GetString("Skin"));
    }
}