﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickListener : MonoBehaviour
{

    public GameObject goController;
    public GameObject menu;
    public GameObject winScreen;
    public GameObject loseScreen;

    public void Up(string name)
    {
        if (menu.activeSelf)
        {
            switch (name)
            {
                case "Exit":
                    Application.Quit(0);
                    break;
                case "Resume":
                    menu.SetActive(false);
                    break;
                case "Main":
                    SceneManager.LoadScene(0);
                    break;
            }
            return;
        }
        if (loseScreen.activeSelf)
        {
            switch (name)
            {
                case "Retry":
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                    break;
            }
            return;
        }
        if (winScreen.activeSelf)
        {
            switch (name)
            {
                case "Retry":
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                    break;
                case "Next":
                    string scene = SceneManager.GetActiveScene().name;
                    int level = int.Parse(scene.Substring(scene.Length - 1)) + 1;
                    scene = scene.Substring(0, scene.Length - 1);
                    SceneManager.LoadScene(scene + level.ToString());
                    break;
                case "Main":
                    SceneManager.LoadScene(0);
                    break;
            }
            return;
        }
        switch (name)
        {
            case "Left":
                goController.GetComponent<PlayerPlatformerController>().hor += 0.01f;
                break;
            case "Right":
                goController.GetComponent<PlayerPlatformerController>().hor += 0.01f;
                break;
            case "Jump":
                goController.GetComponent<PlayerPlatformerController>().jump = 0;
                break;
            case "Menu":
                menu.SetActive(true);
                break;
        }
    }

    public void Down(string name)
    {
        if (menu.activeSelf)
        {
            return;
        }
        if (winScreen.activeSelf)
        {
            return;
        }
        if (loseScreen.activeSelf)
        {
            return;
        }
        switch (name)
        {
            case "Left":
                goController.GetComponent<PlayerPlatformerController>().hor = -1;
                break;
            case "Right":
                goController.GetComponent<PlayerPlatformerController>().hor = 1;
                break;
            case "Jump":
                goController.GetComponent<PlayerPlatformerController>().jump = 1;
                break;
        }
    }
}
