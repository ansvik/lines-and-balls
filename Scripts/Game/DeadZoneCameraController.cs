﻿using UnityEngine;
using System.Collections;

public class DeadZoneCameraController : MonoBehaviour
{

    public GameObject player;
    public Vector2 deadZone;
    private Vector3 offset;
    private Vector2 cPos;
    private Vector2 pPos;


    void Start()
    {
        offset = -(transform.position - player.transform.position);
        cPos = player.transform.position;
    }

    void LateUpdate()
    {
        //transform.position.Set();
        pPos = player.transform.position;
        if (pPos.x < cPos.x - deadZone.x/2)
        {
            transform.position = new Vector3(transform.position.x - (cPos.x - deadZone.x / 2 - pPos.x), transform.position.y, transform.position.z);
        }
        if (pPos.x > cPos.x + deadZone.x/2)
        {
            transform.position = new Vector3(transform.position.x + (pPos.x - cPos.x - deadZone.x / 2), transform.position.y, transform.position.z);
        }
        if (pPos.y < cPos.y - deadZone.y / 2)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (cPos.y - deadZone.y / 2 - pPos.y), transform.position.z);
        }
        if (pPos.y > cPos.y + deadZone.y / 2)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (pPos.y - cPos.y - deadZone.y / 2), transform.position.z);
        }
        cPos = transform.position + offset;
    }
}

