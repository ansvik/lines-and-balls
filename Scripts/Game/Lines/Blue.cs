﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blue : MonoBehaviour
{
    public Transform target;
    public GameObject player;

    private static int teleported;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (teleported == 2) return;
        if (collision.gameObject.name == "Round")
        {
            StartCoroutine(WaitAndTeleport(0.15f));
        }
        teleported++;
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (teleported == 1) teleported = 2;
        else if (teleported == 2) teleported = 0;
    }

    IEnumerator WaitAndTeleport(float time)
    {
        yield return new WaitForSeconds(time);
        player.transform.position = new Vector3(target.position.x, target.position.y + 1, target.position.z);
    }
}
