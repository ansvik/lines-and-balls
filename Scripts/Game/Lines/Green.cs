﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Green : MonoBehaviour
{

    public GameObject winScreen;
    public GameObject player;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Round")
        {
            StartCoroutine(WaitAndShow(0.3f));
            string scene = SceneManager.GetActiveScene().name;
            int level = int.Parse(scene.Substring(scene.Length - 1));
            if (level == PlayerPrefs.GetInt("LevelCount"))
            {
                GameObject.Find("Next").SetActive(false);
                return;
            }
            if (PlayerPrefs.GetInt("OpenLevels") == level)
            {
                PlayerPrefs.SetInt("OpenLevels", level + 1);
            }
        }
    }

    IEnumerator WaitAndShow(float time)
    {
        yield return new WaitForSeconds(time);
        winScreen.SetActive(true);
        player.SetActive(false);
    }
}
