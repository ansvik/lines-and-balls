﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCheck : MonoBehaviour
{

    public GameObject loseScreen;

    void Update()
    {
        if (transform.position.y < -17)
        {
            gameObject.SetActive(false);
            loseScreen.SetActive(true);
        }
    }
}
