﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject
{

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    public float hor = 0;
    public int jump = 0;

    private SpriteRenderer spriteRenderer;
    //private Animator animator;

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        //animator = GetComponent<Animator>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = hor;

        if (jump == 1 && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
            jump = 2;
        }
        else if (jump == 0 && grounded)
        {
            if (velocity.y > 0)
            {
                velocity.y = 0;
            }
        }
        else if (jump == 2 && grounded)
        {
            if (velocity.y > 0)
            {
                velocity.y = 0;
            }
            jump = 1;
        }

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        //animator.SetBool("grounded", grounded);
        //animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;

        if (hor != 1 && hor != -1) hor *= 0.97f;
    }

    void LateUpdate()
    {

    }
}