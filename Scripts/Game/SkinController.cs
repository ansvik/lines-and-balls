﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinController : MonoBehaviour
{
    void Start()
    {
        string skin = PlayerPrefs.GetString("Skin");
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Skins/" + skin);
    }
}
