﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{

    public int levelCount;

    void Start()
    {
        PlayerPrefs.SetInt("LevelCount", levelCount);

        GameObject[] levels = GameObject.FindGameObjectsWithTag("LevelButton");
        foreach (GameObject go in levels)
        {
            go.SetActive(false);
        }
        Debug.Log(PlayerPrefs.GetInt("OpenLevels"));
        if (PlayerPrefs.GetInt("OpenLevels") < 1)
        {
            PlayerPrefs.SetInt("OpenLevels", 1);
        }
        int openLevels = PlayerPrefs.GetInt("OpenLevels");
        if (openLevels > levels.Length)
        {
            openLevels = levels.Length - 1;
        }
        foreach (GameObject go in levels)
        {
            if(int.Parse(go.name) <= openLevels) go.SetActive(true);
        }
    }
}
