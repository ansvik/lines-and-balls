﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    public void OnClick(string level)
    {
        level = "Level" + level;
        SceneManager.LoadScene(level);
    }
}
