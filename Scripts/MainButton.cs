﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainButton : MonoBehaviour
{
    void OnMouseUpAsButton()
    {
        switch (gameObject.name)
        {
            case "Play":
                SceneManager.LoadScene(1);
                break;
            case "Bonus":
                SceneManager.LoadScene(2);
                break;
        }
    }

    void OnMouseDown()
    {

    }
}
